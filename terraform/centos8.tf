resource "proxmox_vm_qemu" "master" {
  count = 1
  name = "CentOS8-server"
  target_node = var.proxmox_host
  vmid = var.vmid.master
  clone = var.template_name
  agent = 1
  os_type = "cloud-init"
  cores = var.cores.master
  sockets = var.sockets.master
  cpu = "host"
  memory = var.memory.master
  scsihw = "virtio-scsi-pci"
  bootdisk = "scsi0"
  pool = var.pool
  
  disk {
    slot = 0
    size = var.disk.master
    type = "scsi"
    storage = var.pve_storage
    iothread = 1
  }
  
  disk {
    slot = 1
    size = var.disk.master
    type = "scsi"
    storage = "100M"
    iothread = 1
  }

  disk {
    slot = 2
    size = var.disk.master
    type = "scsi"
    storage = "100M"
    iothread = 1
  }

  disk {
    slot = 3
    size = var.disk.master
    type = "scsi"
    storage = "100M"
    iothread = 1
  }

  disk {
    slot = 4
    size = var.disk.master
    type = "scsi"
    storage = "100M"
    iothread = 1
  }

  network {
    model = "virtio"
    bridge = "vmbr0"
  }

  lifecycle {
    ignore_changes = [
      network,
    ]
  }

  ipconfig0 = "ip=${var.net_master.ip}/${var.net_master.mask},gw=${var.net_master.gw}"
  sshkeys = <<EOF
  ${var.ssh_pubkey}
  EOF

   provisioner "file" {
      source      = "mdadm.sh"
      destination = "/home/${var.ssh_user}/mdadm.sh"
   connection {
      type        = "ssh"
      user        = var.ssh_user
      host        = var.net_master.ip
      private_key = "${file("${var.ssh_priv_key}")}"
    }
  }

   provisioner "remote-exec" {
        inline = [
          "cd /home/${var.ssh_user}",
          "chmod +x mdadm.sh",
          "sudo ./sqlpass.sh"
        ]
   connection {
        type        = "ssh"
        user        = var.ssh_user
        host        = var.net_slave.ip
        private_key = "${file("${var.ssh_priv_key}")}"
    }
  }
}